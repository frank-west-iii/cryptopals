require_relative "../../lib/hex_to_bytes_converter"

describe HexToBytesConverter do
  describe "#initialize" do
    context "when passed a hex string" do
      it "does not raise an ArgumentError" do
        expect { HexToBytesConverter.new(value: input_string) }.
          not_to raise_error(ArgumentError)
      end
    end

    context "when passed a non hex string" do
      it "raises an ArgumentError" do
        expect { HexToBytesConverter.new(value: "a0283z") }.
          to raise_error(ArgumentError)
      end
    end

  end

  describe "#call" do
    let(:input) { "49276d206b696c6c696e6720796f757220627261696e206c696b6"\
                       "5206120706f69736f6e6f7573206d757368726f6f6d" }
    let(:output) { "I'm killing your brain like a poisonous mushroom" }

    it "xors the decoded buffers together" do
      subject = HexToBytesConverter.new(value: input)

      results = subject.call

      expect(results).to eq(output)
    end
  end
end
