require_relative "../../lib/xor_converter"

describe XorConverter do
  describe "#initialize" do
    context "when passed buffers of equal lengths" do
      it "does not raise an ArgumentError" do
        expect { XorConverter.
                 new(buffer_one: "0123", buffer_two: "4567") }.
          not_to raise_error(ArgumentError)
      end

    end

    context "when passed buffers of different lengths" do
      it "raises an ArgumentError" do
        expect { XorConverter.
                 new(buffer_one: "0123", buffer_two: "456") }.
          to raise_error(ArgumentError)
      end
    end
  end

  describe "#call" do
    let(:buffer_one) { "1c0111001f010100061a024b53535009181c" }
    let(:buffer_two) { "686974207468652062756c6c277320657965" }
    let(:output) { "746865206b696420646f6e277420706c6179" }

    it "xors the decoded buffers together" do
      subject = XorConverter.new(buffer_one: buffer_one, buffer_two: buffer_two)

      results = subject.call

      expect(results).to eq(output)
    end
  end
end
