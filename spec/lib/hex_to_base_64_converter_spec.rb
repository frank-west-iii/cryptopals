require_relative "../../lib/hex_to_base_64_converter"
require_relative "../../lib/hex_to_bytes_converter"

describe HexToBase64Converter do
  let(:input) { HexToBytesConverter.new(value: "49276d206b696c6c696e6720796f75"\
                                        "7220627261696e206c696b65206120706f697"\
                                        "36f6e6f7573206d757368726f6f6d").call }
  let(:output) { "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzI"\
                        "G11c2hyb29t" }

  describe "#call" do
    it "converts it to a base64 string" do
      subject = HexToBase64Converter.new(value: input)

      results = subject.call

      expect(results).to eq(output)
    end
  end
end
