class XorConverter
  def initialize(buffer_one:, buffer_two:)
    if buffer_one.length != buffer_two.length
      raise ArgumentError.new("Buffers must be equal lengths")
    end
    @buffer_one = buffer_one
    @buffer_two = buffer_two
  end

  def call
    hex_xor_value
  end

  private

  attr_reader :buffer_one, :buffer_two

  def hex_xor_value
    @_hex_xor_value ||= xor_values.map { |value| value.to_s(16) }.join
  end

  def xor_values
    decoded_buffer_one.bytes.zip(decoded_buffer_two.bytes).map do |x,y|
      x ^ y
    end
  end

  def decoded_buffer_one
    HexToBytesConverter.new(value: buffer_one).call
  end

  def decoded_buffer_two
    HexToBytesConverter.new(value: buffer_two).call
  end
end
