class HexToBytesConverter
  def initialize(value:)
    @value = value
    unless is_valid_hex?
      raise ArgumentError.new("#{value} is not valid hex")
    end
  end

  def call
    decoded_hex
  end

  private

  attr_reader :value

  def is_valid_hex?
    /\A[A-Fa-f0-9]+\z/ =~ value
  end

  def decoded_hex
    [value].pack("H*")
  end
end
