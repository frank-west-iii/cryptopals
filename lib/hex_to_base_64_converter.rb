require "base64"

class HexToBase64Converter
  def initialize(value:)
    @value = value
  end

  def call
    encoded_base_64
  end

  private

  attr_reader :value

  def encoded_base_64
    @_encoded_base_64 ||= Base64.strict_encode64(value)
  end

end
